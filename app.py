# -*- coding: utf-8 -*-

import json
from flask import Flask, redirect, make_response

app = Flask(__name__)


@app.route('/api/v1/authorize', methods=['GET'])
def authorize():
    # https://app.bigbird.ru/?code=HFUhuIZsLLYFMgCzIb8ANqUKSRMKqJTx
    # return redirect("http://127.0.0.1:8999/tochka?code=lmnMZexB3ANDLf7na1Xlm9i8VO9ZlgMm", code=302)
    return redirect("http://127.0.0.1:8999/tochka?code=lmnMZexB3ANDLf7na1Xlm9i8VO9ZlgMm", code=302)

@app.route('/api/v1/oauth2/token', methods=['POST'])
def token():
    data = {
        "refresh_token": "jPU5Ccnecwb7VuMXAAtFCOETIGagyxvr",
        "token_type": "bearer",
        "access_token": "XCFKBUvvHyNv6Q3XSnkeQbBlSMR8mk5B",
        "expires_in": 86400
    }
    response = make_response(json.dumps(data))
    return response

@app.route('/api/v1/account/list', methods=['GET'])
def list():
    data = {
        "bank_code": "044525797",
        "code": "40702810710050039378"
    }
    response = make_response(json.dumps(data))
    return response

@app.route('/api/v1/statement', methods=['POST'])
def statement():
    data = {
        "request_id": "044525999.2018-01-01.2018-02-01.40817810102500000169"
    }
    response = make_response(json.dumps(data))
    return response

@app.route('/v1/statement/status/<request_id>', methods=['GET'])
def status():
    data = {
        "status": "ready"
    }
    response = make_response(json.dumps(data))
    return response

@app.route('/api/v1/statement/result/<id>', methods=['GET'])
def result():
    data = {
        "balance_opening": "0",
        "balance_closing": "948.67",
        "payments": [
            {
                "counterparty_account_number": "40702810538000087136",
                "counterparty_bank_bic": "044525225",
                "counterparty_bank_name": u"ПАО СБЕРБАНК, Москва",
                "counterparty_inn": "7701904600",
                "counterparty_kpp": "770101001",
                "counterparty_name": u"ООО \"ЭТЕРОН\"",
                "operation_type": "1",
                "payment_amount": "500",
                "payment_bank_system_id": "71;261763839",
                "payment_charge_date": "17.07.2018",
                "payment_date": "17.07.2018",
                "payment_number": "361",
                "payment_purpose": "Перевод денежных средств на собственный счет в другом банке. НДС не облагается.",
                "supplier_bill_id": "",
                "tax_info_document_date": "",
                "tax_info_document_number": "",
                "tax_info_kbk": "",
                "tax_info_okato": "",
                "tax_info_period": "",
                "tax_info_reason_code": "",
                "tax_info_status": "",
                "x_payment_id": "71;261763839;215196698;1"
            },
            {
                "counterparty_account_number": "40702810900001425411",
                "counterparty_bank_bic": "044525700",
                "counterparty_bank_name": u"АО \"РАЙФФАЙЗЕНБАНК\", Москва",
                "counterparty_inn": "7715356456",
                "counterparty_kpp": "770201001",
                "counterparty_name": u"ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ \"СПСР-ЭКСПРЕСС\"",
                "operation_type": "1",
                "payment_amount": "-454.3",
                "payment_bank_system_id": "71;261768052",
                "payment_charge_date": "17.07.2018",
                "payment_date": "17.07.2018",
                "payment_number": "1",
                "payment_purpose": u"Оплата согласно Договора № 6700046511 от 15.03.2011, по счету № 10320640/670 от 15.07.2018 за услуги по доставке \"Колибри-документ\", в т.ч. НДС 18% 69,30",
                "supplier_bill_id": "0",
                "tax_info_document_date": "",
                "tax_info_document_number": "",
                "tax_info_kbk": "",
                "tax_info_okato": "",
                "tax_info_period": "",
                "tax_info_reason_code": "",
                "tax_info_status": "",
                "x_payment_id": "71;261768052;215198511;2"
            },
            {
                "counterparty_account_number": "70601810810052720216",
                "counterparty_bank_bic": "044525797",
                "counterparty_bank_name": u"ТОЧКА КИВИ БАНК (АО), Москва",
                "counterparty_inn": "3123011520",
                "counterparty_kpp": "772643001",
                "counterparty_name": u"Филиал Точка Банк КИВИ Банк (акционерное общество)",
                "operation_type": "17",
                "payment_amount": "-45.7",
                "payment_bank_system_id": "71;262445770",
                "payment_charge_date": "17.07.2018",
                "payment_date": "17.07.2018",
                "payment_number": "794742",
                "payment_purpose": u"Оплата тарифного плана \"Идеальный\" за ведение счёта с 16.07.18 по 31.07.18. Списывается после проведения первой операции, согласно Правилам банковского обслуживания. НДС не предусмотрен.\n\n",
                "supplier_bill_id": "",
                "tax_info_document_date": "",
                "tax_info_document_number": "",
                "tax_info_kbk": "",
                "tax_info_okato": "",
                "tax_info_period": "",
                "tax_info_reason_code": "",
                "tax_info_status": "",
                "x_payment_id": "71;262445770;215784203;2"
            },
            {
                "counterparty_account_number": "40702810538000087136",
                "counterparty_bank_bic": "044525225",
                "counterparty_bank_name": u"ПАО СБЕРБАНК, Москва",
                "counterparty_inn": "7701904600",
                "counterparty_kpp": "770101001",
                "counterparty_name": u"ООО \"ЭТЕРОН\"",
                "operation_type": "1",
                "payment_amount": "500",
                "payment_bank_system_id": "71;262638726",
                "payment_charge_date": "18.07.2018",
                "payment_date": "18.07.2018",
                "payment_number": "364",
                "payment_purpose": u"Перевод денежных средств на собственный счет в другом банке. НДС не облагается.",
                "supplier_bill_id": "",
                "tax_info_document_date": "",
                "tax_info_document_number": "",
                "tax_info_kbk": "",
                "tax_info_okato": "",
                "tax_info_period": "",
                "tax_info_reason_code": "",
                "tax_info_status": "",
                "x_payment_id": "71;262638726;215937521;1"
            },
            {
                "counterparty_account_number": "47423810310050027499",
                "counterparty_bank_bic": "044525797",
                "counterparty_bank_name": u"ТОЧКА КИВИ БАНК (АО), Москва",
                "counterparty_inn": "3123011520",
                "counterparty_kpp": "",
                "counterparty_name": u"Ф Точка Банк КИВИ Банк (АО)",
                "operation_type": "2",
                "payment_amount": "-212.36",
                "payment_bank_system_id": "71;262446705",
                "payment_charge_date": "18.07.2018",
                "payment_date": "17.07.2018",
                "payment_number": "122614",
                "payment_purpose": u"Оплата тарифного плана \"Идеальный\" за ведение счёта с 16.07.18 по 31.07.18. Списывается после проведения первой операции, согласно Правилам банковского обслуживания. НДС не предусмотрен. ",
                "supplier_bill_id": "",
                "tax_info_document_date": "",
                "tax_info_document_number": "",
                "tax_info_kbk": "",
                "tax_info_okato": "",
                "tax_info_period": "",
                "tax_info_reason_code": "",
                "tax_info_status": "",
                "x_payment_id": "71;262446705;215938028;2"
            },
            {
                "counterparty_account_number": "40702810538000087136",
                "counterparty_bank_bic": "044525225",
                "counterparty_bank_name": "ПАО СБЕРБАНК, Москва",
                "counterparty_inn": "7701904600",
                "counterparty_kpp": "770101001",
                "counterparty_name": u"ООО \"ЭТЕРОН\"",
                "operation_type": "1",
                "payment_amount": "20000",
                "payment_bank_system_id": "71;266987557",
                "payment_charge_date": "25.07.2018",
                "payment_date": "25.07.2018",
                "payment_number": "368",
                "payment_purpose": u"Перевод денежных средств на собственный счет в другом банке. НДС не облагается.",
                "supplier_bill_id": "",
                "tax_info_document_date": "",
                "tax_info_document_number": "",
                "tax_info_kbk": "",
                "tax_info_okato": "",
                "tax_info_period": "",
                "tax_info_reason_code": "",
                "tax_info_status": "",
                "x_payment_id": "71;266987557;219374988;1"
            },
            {
                "counterparty_account_number": "40702810996430000358",
                "counterparty_bank_bic": "044525256",
                "counterparty_bank_name": u"ПАО РОСБАНК, Москва",
                "counterparty_inn": "7706279935",
                "counterparty_kpp": "710701001",
                "counterparty_name": u"ООО \"КОМУС-Р2\"",
                "operation_type": "1",
                "payment_amount": "-6017.95",
                "payment_bank_system_id": "71;267006457",
                "payment_charge_date": "25.07.2018",
                "payment_date": "25.07.2018",
                "payment_number": "2",
                "payment_purpose": u"Оплата по счету № OVT/2135598/21101531  от 18.07.2018 г за канцелярские и хозяйственные товары в т.ч. НДС 18% 850,56 , НДС 10% 40,18",
                "supplier_bill_id": "0",
                "tax_info_document_date": "",
                "tax_info_document_number": "",
                "tax_info_kbk": "",
                "tax_info_okato": "",
                "tax_info_period": "",
                "tax_info_reason_code": "",
                "tax_info_status": "",
                "x_payment_id": "71;267006457;219385429;2"
            },
            {
                "counterparty_account_number": "40702810000000005790",
                "counterparty_bank_bic": "044525104",
                "counterparty_bank_name": u"АО БАНК ИННОВАЦИЙ И РАЗВИТИЯ, Москва",
                "counterparty_inn": "9701054392",
                "counterparty_kpp": "770101001",
                "counterparty_name": u"ООО \"АРЕНА-ТЕЛЕКОМ\"",
                "operation_type": "1",
                "payment_amount": "-13321.02",
                "payment_bank_system_id": "71;267010047",
                "payment_charge_date": "25.07.2018",
                "payment_date": "25.07.2018",
                "payment_number": "3",
                "payment_purpose": u"Оплата по счету № 555 от 30 июня 2018 г .за телекоммуникационные услуги за Июнь 2018 г .без НДС",
                "supplier_bill_id": "0",
                "tax_info_document_date": "",
                "tax_info_document_number": "",
                "tax_info_kbk": "",
                "tax_info_okato": "",
                "tax_info_period": "",
                "tax_info_reason_code": "",
                "tax_info_status": "",
                "x_payment_id": "71;267010047;219386901;2"
            }
        ]
    }
    response = make_response(json.dumps(data))
    return response


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=2000)